<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Parser job info</title>
    <link rel="stylesheet" href="/static/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="/static/bower_components/bootstrap/dist/css/bootstrap-theme.min.css">
</head>
<body>

<nav class="navbar navbar-inverse">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="/">Parser job info</a>
        </div>
    </div>
</nav>

<div class="container">

    <form action="/parser.php" method="POST">

        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                    <input type="text" name="url" class="form-control" value="http://job-openings.monster.com/Senior-Software-Engineer-Infrastructure-Platform-San-Francisco-CA-US-Affirm/31/77035e96-ab0e-45cc-9497-b2c72621ba88" id="url" placeholder="Введите адрес страницы">
                </div>
            </div>
            <div class="col-md-2">
                <button type="submit" class="btn btn-primary">Распарсить</button>
            </div>
        </div>

        <div id="result">

        </div>

    </form>
</div>


    <script src="/static/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="/static/js/common.js"></script>
</body>
</html>
