$(function () {

    var parser = ({

        init: function () {

            this.$form = $('form');
            this.$result = $('#result');


            this.bind();

            return this;
        },

        send: function () {

            var _this = this;

            _this.$result.html( '<div>Загрузка...</div>' );

            $.post( _this.$form.attr('action'), _this.$form.serialize(), function( data ) {

                _this.$result.html( data );

            }).fail(function() {

                alert( "Возникла ошибка на сервере" );

            });

        },

        bind: function () {

            var _this = this;


            _this.$form.submit(function () {

                _this.send();

                return false;
            });


        }


    }).init();


});