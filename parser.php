<?php
require __DIR__ . '/vendor/autoload.php';
require __DIR__.'/Helpers/ParserClass.php';
require __DIR__.'/Helpers/ViewClass.php';


$parser = new \Helpers\ParserClass($_POST['url']);
$view = new \Helpers\ViewClass();

$data = $parser->parse();

if($data){
    echo $view->render('/views/job_info.php', $data);
}else{
    echo $view->render('/views/error.php', []);
}