<?php
namespace Helpers;

use Sunra\PhpSimple\HtmlDomParser;
use \Curl\Curl;

class ParserClass
{

    public $url;
    protected $dom;

    function __construct($url)
    {
        $this->url = $url;
    }


    /**
     * @return array
     */
    public function parse()
    {

        $this->dom = $this->getDom($this->url);

        if (!$this->dom) return [];

        return [
            'job_name' => $this->getJobName(),
            'job_info' => $this->getJobInfo(),

            'company_name' => $this->getCompanyName(),
            'company_location' => $this->getCompanyLocation(),
        ];
    }




    public function getJobName()
    {
        $title = explode(' at ', $this->getTitle());

        return $title[0];
    }

    /**
     * @return string
     */
    public function getJobInfo()
    {

        return $this->getTextBySelector('div#JobDescription');
    }

    /**
     * @return string
     */

    public function getCompanyLocation()
    {
        return $this->getValueSummarySectionByKey('Location');

    }


    public function getCompanyName()
    {

        $company_name = $this->getTextBySelector('#AboutCompany h3.name');

        if(!$company_name){
            $title = explode(' at ', $this->getTitle());
            $company_name = isset($title[1]) ? $title[1] : '';
        }

        return $company_name;

    }


    /**
     * @return string
     */
    public function getTitle()
    {

        return $this->getTextBySelector('h1.title');

    }

    /**
     * @param $selector
     * @return string
     */
    protected function getTextBySelector($selector)
    {

        $info = $this->dom->find($selector);

        return isset($info[0]) ? $info[0]->innertext : '';
    }

    /**
     * Находим значение по тексту ключа
     * @param $key
     * @return string
     */
    protected function getValueSummarySectionByKey($key)
    {
        $value = '';

        foreach ($this->dom->find('.mux-job-summary .summary-section') as $section)
        {
            if($section->find('.key') && $section->find('.key', 0)->plaintext == $key){
                $value = $section->find('.value', 0)->plaintext;
            }
        }

        return $value;
    }


    /**
     * @param $url
     * @return \simplehtmldom_1_5\simple_html_dom
     */
    protected function getDom($url)
    {
        return HtmlDomParser::str_get_html( $this->getHtml( $url ) );
    }

    /**
     * @param $url
     * @return null
     */
    protected function getHtml($url)
    {
        $curl = new Curl();
        $curl->get($url);

        if ($curl->error) {
            echo 'Error: ' . $curl->errorCode . ': ' . $curl->errorMessage . "\n";
            exit();
        } else {
            return $curl->response;
        }


    }





}